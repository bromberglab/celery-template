import os
import subprocess

def run_command(command, shell=False, print_output=True, env_exports={}):
    current_env = os.environ.copy()
    merged_env = {**current_env, **env_exports}
    process = subprocess.Popen(command, shell=shell, env=merged_env, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout = []
    stdout_data, stderr_data = process.communicate()
    for line in stdout_data.splitlines():
        line = line.rstrip().decode('utf8')
        if print_output:
            print(f'shell> {line}')
        stdout.append(line)
    if process.returncode != 0:
        stderr = []
        stderr_data = '' if not stderr_data else stderr_data
        for line in stderr_data.splitlines():
            line = line.rstrip().decode('utf8')
            stderr.append(line)
        print(f'Error while executing command: {" ".join(stderr)}')
    return stdout
