import os
import shutil
import urllib.request
import urllib.parse
from datetime import datetime
from pathlib import Path
from .helpers import run_command
from .worker import app


HOSTNAME = os.getenv('HOSTNAME', 'celery-worker')
APP_PATH = Path(os.getenv('APP_PATH', '/app'))
APP_SCRATCH = Path(os.getenv('APP_SCRATCH', '/scratch')) / 'celery-worker'
KEEP_APP_SCRATCH_DAYS = int(os.getenv('KEEP_APP_SCRATCH_DAYS', 7))


@app.task(bind=True, name='cron', queue='beat')
def cron(self, tasks):  
    for task in tasks:
        print(f'got task: {task}')
        if task == 'cleanup':
            for sfolder in APP_SCRATCH.glob('*'):
                createTime = datetime.fromtimestamp(sfolder.stat().st_atime)
                delta = datetime.now() - createTime
                if delta.days > KEEP_APP_SCRATCH_DAYS:
                    shutil.rmtree(sfolder)
        elif task == 'beat':
            print(f'Got beat: {HOSTNAME}')


@app.task(bind=True, name='app.submit')  
def submit(self, *args, **kwargs):

    # process taks
    print(f'New local submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['submission_id']

    status = "ok"

    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')
